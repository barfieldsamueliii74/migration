#!/usr/bin/env bash

set -euo pipefail
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
UNSYMLINKED_SCRIPT_DIR="$(readlink -f "${SCRIPT_DIR}" || readlink "${SCRIPT_DIR}" || echo "${SCRIPT_DIR}")"
# shellcheck disable=SC1091,SC1090
source "${UNSYMLINKED_SCRIPT_DIR}/../../../../workflow-script-commons.sh"

if [[ -z $POSTGRESQL_AZURE_SECONDARIES ]]; then
  echo "You must set POSTGRESQL_AZURE_SECONDARIES in source_vars"
fi

for secondary in ${POSTGRESQL_AZURE_SECONDARIES[*]}
do
    recovery=$(ssh_host "$secondary" "sudo gitlab-psql  -t -d gitlab_repmgr -c 'select pg_is_in_recovery();'")
    echo "$secondary : pg_is_in_recovery=$recovery"
done

echo "postgresql will be shutdown on the above hosts, press enter to continue"
read -r

echo "Shutting down:"
for secondary in ${POSTGRESQL_AZURE_SECONDARIES[*]}
do
    ssh_host "$secondary" "sudo gitlab-ctl stop postgresql"
done

echo "Waiting an appropriate time..."
sleep 5

echo "Getting status:"
for secondary in ${POSTGRESQL_AZURE_SECONDARIES[*]}
do
    p_status=$(ssh_host "$secondary" "sudo gitlab-ctl status postgresql")
    echo "$secondary: $p_status"
done




